package com.example.pmrowiec.galleryfinal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;

public class CollageDirectoryListActivity extends AppCompatActivity {

    private LinearLayout layout;
    private Button addFolder;
    private Button deleteAll;

    public void refresh() {
        super.onResume();
        this.onCreate(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory_list);

        Typeface fontType = Typeface.createFromAsset(getAssets(),"fonts/fontawesome-webfont.ttf");

        layout = (LinearLayout) findViewById(R.id.directoryList);
        addFolder = (Button) findViewById(R.id.addFolder);
        deleteAll = (Button) findViewById(R.id.deleteAll);

        addFolder.setTypeface(fontType);
        deleteAll.setTypeface(fontType);

        addFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(CollageDirectoryListActivity.this);
                alertDialog.setTitle("Wprowadź nazwę galerii");

                final EditText input = new EditText(CollageDirectoryListActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);

                alertDialog.setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String name = input.getText().toString();
                                DirectoryClass dir = new DirectoryClass(name);

                                dir.create();
                                refreshDirectories();
                                dialog.cancel();
                            }
                        });

                alertDialog.setNegativeButton("Anuluj",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();
            }
        });

        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DirectoryClass dir = new DirectoryClass();

                dir.deleteSelf();
                refreshDirectories();
                Toast.makeText(getApplicationContext(), "Dane zostały usunięte.",
                        Toast.LENGTH_SHORT).show();

            }
        });

        loadDirectories();
    }

    private void loadDirectories(){
        DirectoryClass dir = new DirectoryClass();
        File folder = dir.getMainDir();

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                ListItemClass item = new ListItemClass(CollageDirectoryListActivity.this, layout, fileEntry, this);

                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CollageDirectoryListActivity.this, CollagePhotoLIstActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
                        intent.putExtra("dirPath", fileEntry.getAbsolutePath());
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }
    }

    public void refreshDirectories(){
        if(((LinearLayout) layout).getChildCount() > 0){
            ((LinearLayout) layout).removeAllViews();

            loadDirectories();
        }
    }
}
