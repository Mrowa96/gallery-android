package com.example.pmrowiec.galleryfinal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ColorPickerClass extends RelativeLayout {

    public int color;

    public ColorPickerClass(final Context context, final RelativeLayout parentLayout, final String type){
        super(context);

        final RelativeLayout that = this;

        ImageView colorImage = new ImageView(context);
        Button acceptColor = new Button(context);

        colorImage.setImageResource(R.drawable.color);
        colorImage.setDrawingCacheEnabled(true);

        acceptColor.setText("Ustaw");

        this.setLayoutParams(new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        this.setBackgroundColor(Color.GRAY);
        this.invalidate();
        this.addView(colorImage);
        this.addView(acceptColor);

        parentLayout.addView(this);

        acceptColor.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                that.setVisibility(INVISIBLE);
            }
        });

        colorImage.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_MOVE:
                        Bitmap bmp = v.getDrawingCache();

                        color = bmp.getPixel((int) event.getX(), (int) event.getY());

                        ((FontActivity) context).changeColor(color, type);
                        break;

                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                }
                return true;
            }
        });

    }
}

