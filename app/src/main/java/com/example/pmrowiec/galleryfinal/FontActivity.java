package com.example.pmrowiec.galleryfinal;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.IOException;

public class FontActivity extends AppCompatActivity {

    Typeface selectedFont;
    PreviewTextClass previewText;
    int firstColor = Color.BLACK;
    int secondColor = Color.GREEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_font);

        final RelativeLayout fontPreview = (RelativeLayout) findViewById(R.id.fontPreview);
        final EditText editText = (EditText) findViewById(R.id.editText);
        final RelativeLayout fontWrap = (RelativeLayout) findViewById(R.id.fontWrap);
        LinearLayout fontList = (LinearLayout) findViewById(R.id.fontList);
        Button colorPicker1 = (Button) findViewById(R.id.colorPicker1);
        Button colorPicker2 = (Button) findViewById(R.id.colorPicker2);
        AssetManager assetManager = getAssets();

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = String.valueOf(editText.getText());
                previewText = new PreviewTextClass(FontActivity.this, 60,selectedFont, text , firstColor, secondColor);

                fontPreview.removeAllViews();
                fontPreview.addView(previewText);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }
        });

        try {
            final String[] lista = assetManager.list("fonts");

            for (int i = 0; i< lista.length; i++){
                TextView fontName = new TextView(this);
                Typeface fontType = Typeface.createFromAsset(getAssets(),"fonts/"+lista[i].toString());

                fontName.setText(lista[i].toString());
                fontName.setTypeface(fontType);
                fontName.setTextSize(40);
                fontName.setId(i);

                fontName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedFont = Typeface.createFromAsset(getAssets(),"fonts/" + lista[v.getId()]);
                    }
                });

                fontList.addView(fontName);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        colorPicker1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerClass colorPicker = new ColorPickerClass(FontActivity.this, fontWrap, "color1");
            }
        });

        colorPicker2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerClass colorPicker2 = new ColorPickerClass(FontActivity.this, fontWrap, "color2");
            }
        });
    }

    public void changeColor(int color, String type) {
        switch(type){
            case "color1":
                firstColor = color;
                break;

            case "color2":
                secondColor = color;
                break;
        }
    }
}
