package com.example.pmrowiec.galleryfinal;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;

public class ImageEditionClass {
    private float[] normalTab = {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 1, 0
    };
    private float[] redTab = {
            2, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 1, 0
    };
    private float[] greenTab = {
            0, 0, 0, 0, 0,
            0, 2, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 1, 0
    };

    public void rotate(ImageView image) {
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        Bitmap b = ((BitmapDrawable) image.getDrawable()).getBitmap();
        Bitmap rotated = Bitmap.createBitmap(b, 0, 0,b.getWidth(), b.getHeight(),matrix, true);

        image.setImageBitmap(rotated);
    }

    public int flip(ImageView image, int type) {
        Matrix matrix = new Matrix();
        int returnedType = type;

        switch(type){
            case 1:
                matrix.postScale(-1.0f, 1.0f);
                returnedType = 2;
                break;
            case 2:
                matrix.postScale(1.0f, -1.0f);
                returnedType = 1;
                break;
        }

        Bitmap b = ((BitmapDrawable) image.getDrawable()).getBitmap();
        Bitmap flipped = Bitmap.createBitmap(b, 0, 0,b.getWidth(), b.getHeight(),matrix, true);

        image.setImageBitmap(flipped);

        return returnedType;
    }

    public void color(ImageView image, String type){
        ColorMatrix cMatrix = new ColorMatrix();

        switch(type){
            case "Czerwony":
                cMatrix.set(redTab);
                break;
            case "Zielony":
                cMatrix.set(greenTab);
                break;
        }

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Bitmap b = ((BitmapDrawable) image.getDrawable()).getBitmap();
        Bitmap copyBitmap = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());

        Canvas canvas = new Canvas(copyBitmap);
        canvas.drawBitmap(b, 0, 0, paint);

        image.setImageBitmap(copyBitmap);
    }

    public void brightness(ImageView image, float brightness){
        ColorMatrix cMatrix = new ColorMatrix();

        brightness = brightness/100;

        float[] brightnessTab = {
                1, 0, 0, 0, brightness,
                0, 1, 0, 0, brightness,
                0, 0, 1, 0, brightness,
                0, 0, 0, 1, 0 };

        cMatrix.set(brightnessTab);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Bitmap b = ((BitmapDrawable) image.getDrawable()).getBitmap();
        Bitmap copyBitmap = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());

        Canvas canvas = new Canvas(copyBitmap);
        canvas.drawBitmap(b, 0, 0, paint);

        image.setImageBitmap(copyBitmap);
    }

    public void contrast(ImageView image, float contrast){
        ColorMatrix cMatrix = new ColorMatrix();

        contrast = contrast/100;

        float[] contrast_tab = {
                contrast, 0, 0, 0, 0,
                0, contrast, 0, 0, 0,
                0, 0, contrast, 0, 0,
                0, 0, 0, 1, 0 };

        cMatrix.set(contrast_tab);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Bitmap b = ((BitmapDrawable) image.getDrawable()).getBitmap();
        Bitmap copyBitmap = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());

        Canvas canvas = new Canvas(copyBitmap);
        canvas.drawBitmap(b, 0, 0, paint);

        image.setImageBitmap(copyBitmap);
    }

    public void saturation(ImageView image, float saturation){
        ColorMatrix cMatrix = new ColorMatrix();
        cMatrix.setSaturation(saturation);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cMatrix));

        Bitmap b = ((BitmapDrawable) image.getDrawable()).getBitmap();
        Bitmap copyBitmap = Bitmap.createBitmap(b.getWidth(), b.getHeight(), b.getConfig());

        Canvas canvas = new Canvas(copyBitmap);
        canvas.drawBitmap(b, 0, 0, paint);

        image.setImageBitmap(copyBitmap);
    }

}
