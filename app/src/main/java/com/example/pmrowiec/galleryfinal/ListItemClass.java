package com.example.pmrowiec.galleryfinal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.lang.reflect.Method;

public class ListItemClass extends LinearLayout {

    private Context context;
    private LinearLayout layout;
    private File element;
    private DirectoryListActivity activity = null;
    private CollageDirectoryListActivity activity2 = null;

    public ListItemClass(final Context context, LinearLayout layout, final File element, final DirectoryListActivity activity) {
        super(context);

        this.context = context;
        this.layout = layout;
        this.element = element;
        this.activity = activity;

        doThings();
    }

    public ListItemClass(final Context context, LinearLayout layout, final File element, final CollageDirectoryListActivity activity2) {
        super(context);

        this.context = context;
        this.layout = layout;
        this.element = element;
        this.activity2 = activity2;

        doThings();
    }

    private void doThings(){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        this.setLayoutParams(layoutParams);
        this.setOrientation(LinearLayout.HORIZONTAL);
        this.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final String[] optionsArr = new String[1];
                optionsArr[0] = "Usuń";

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Operacje");
                alertDialogBuilder.setItems(optionsArr, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                DirectoryClass dir = new DirectoryClass(element.getName());
                                dir.deleteSelf();

                                if (activity != null) {
                                    activity.refreshDirectories();
                                } else if (activity2 != null) {
                                    activity2.refreshDirectories();
                                }

                                break;
                        }
                    }
                });

                alertDialogBuilder.show();

                return true;
            }
        });

        if(element.isDirectory()){
            ImageView thumb = new ImageView(context);
            TextView title = new TextView(context);

            LinearLayout.LayoutParams thumbParams = new LinearLayout.LayoutParams(80, 80);
            thumbParams.setMargins(0, 0, 10, 0);
            thumb.setLayoutParams(thumbParams);
            thumb.setImageResource(R.drawable.folder);

            LinearLayout.LayoutParams titleParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            title.setLayoutParams(titleParams);
            title.setText(element.getName());
            title.setGravity(Gravity.CENTER);

            this.addView(thumb);
            this.addView(title);
        }

        layout.addView(this);
    }
}
