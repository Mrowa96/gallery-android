package com.example.pmrowiec.galleryfinal;

import android.os.Environment;
import android.util.Log;

import org.w3c.dom.Element;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DirectoryClass {

    private String[] defaultFolders = {"Miejsca", "Osoby", "Przedmioty"};
    private String path;
    private String directoryName;
    private String prefix = "pmr";

    public DirectoryClass(){
        File mainCatalog = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        this.path = mainCatalog.getAbsolutePath() + "/" + prefix + "/";
    }

    public DirectoryClass(String name){
        File mainCatalog = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        this.path = mainCatalog.getAbsolutePath() + "/" + prefix + "/";
        this.directoryName = name;
    }

    public DirectoryClass(String name, Boolean absolute){
        if(absolute){
            this.path = name;
        }
    }

    public void create(){
        File file = new File(path + directoryName);

        if(!file.exists()) {
            file.mkdirs();
        }
    }

    public void createDefault(){
        for(String dirName : defaultFolders){
            File dir = new File(path + dirName);

            if(!dir.exists()) {
                dir.mkdirs();
            }
        }
    }

    public File getMainDir(){
        File file = new File(path);

        return file;
    }

    public void deleteSelf(){
        File folder;

        if(directoryName != null){
             folder = new File(path + directoryName);
        }
        else{
             folder = new File(path);
        }

        File[] files = folder.listFiles();

        if(files != null) {
            for(File f: files) {
                if(java.util.Arrays.asList(defaultFolders).indexOf(f.getName()) == -1) {
                    if(f.isDirectory()){
                        for(File fc: f.listFiles()) {
                            fc.delete();
                        }
                    }

                    f.delete();
                }
                else{
                    if(f.isDirectory()){
                        for(File fc: f.listFiles()) {
                            fc.delete();
                        }
                    }
                }
            }
        }

        if(directoryName != null){
            folder.delete();
        }
    }

    public List<File> listFiles(){
        File folder = new File(path);
        File[] files = folder.listFiles();

        List<File> list = Arrays.asList(files);

        return list;
    }
}
