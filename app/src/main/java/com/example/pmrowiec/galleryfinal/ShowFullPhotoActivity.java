package com.example.pmrowiec.galleryfinal;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ShowFullPhotoActivity extends AppCompatActivity {

    private ImageView fullImage;
    private String path;
    private String dirPath;
    private ImageEditionClass imageEdition;
    private int flipType = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_full_photo);

        setView();
        Intent intent = getIntent();
        path = String.valueOf(intent.getStringExtra("photo"));
        dirPath = String.valueOf(intent.getStringExtra("dirPath"));

        imageEdition = new ImageEditionClass();

        fullImage = (ImageView)findViewById(R.id.selectImageView);
        Picasso.with(getApplicationContext()).load(new File(path)).into(fullImage);
    }

    private void setView(){
        Typeface fontType = Typeface.createFromAsset(getAssets(),"fonts/fontawesome-webfont.ttf");

        final LinearLayout seeksBars = (LinearLayout) findViewById(R.id.seeksBars);
        final SeekBar brightnessBar = (SeekBar) findViewById(R.id.brightnessControl);
        final SeekBar saturationBar = (SeekBar) findViewById(R.id.saturationControl);
        final SeekBar contrastBar = (SeekBar) findViewById(R.id.contrastControl);

        brightnessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                imageEdition.brightness(fullImage, brightnessBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        contrastBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                imageEdition.contrast(fullImage, contrastBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        saturationBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar arg0, int val, boolean arg2) {
                imageEdition.saturation(fullImage, saturationBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Button showSeekBars = (Button) findViewById(R.id.showSeekBars);
        showSeekBars.setTypeface(fontType);
        showSeekBars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seeksBars.setVisibility(View.VISIBLE);
            }
        });

        Button trash = (Button) findViewById(R.id.trashFull);
        trash.setTypeface(fontType);
        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ShowFullPhotoActivity.this);
                alertDialogBuilder.setTitle(getString(R.string.delete));
                alertDialogBuilder.setMessage(getString(R.string.deleteinfo));

                alertDialogBuilder.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        File file = new File(path);
                        file.delete();
                        finish();
                    }
                });

                alertDialogBuilder.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Button editFull = (Button)findViewById(R.id.editFull);
        editFull.setTypeface(fontType);
        Button rotateBtn = (Button)findViewById(R.id.rotateBtn);
        rotateBtn.setTypeface(fontType);
        Button flipBtn = (Button)findViewById(R.id.flipBtn);
        flipBtn.setTypeface(fontType);
        Button changeColorBtn = (Button)findViewById(R.id.changeColorBtn);
        changeColorBtn.setTypeface(fontType);
        Button shareBtn = (Button)findViewById(R.id.shareBtn);
        shareBtn.setTypeface(fontType);
        final Button accept = (Button)findViewById(R.id.accept);
        accept.setTypeface(fontType);

        editFull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShowFullPhotoActivity.this, FontActivity.class);

                startActivityForResult(intent, 1);
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File photo = new File(path);

                try {
                    String image = MediaStore.Images.Media.insertImage(ShowFullPhotoActivity.this.getContentResolver(), photo.getAbsolutePath(), photo.getName(), null);
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/jpeg");
                    share.putExtra(Intent.EXTRA_STREAM, Uri.parse(image));

                    startActivity(Intent.createChooser(share, "Udostępnij"));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        rotateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageEdition.rotate(fullImage);
            }
        });

        flipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipType = imageEdition.flip(fullImage, flipType);
            }
        });

        changeColorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String [] filterList = new String[2];
                filterList[0] = "Czerwony";
                filterList[1] = "Zielony";

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ShowFullPhotoActivity.this);
                alertDialogBuilder.setTitle("Wybierz filtr");
                alertDialogBuilder.setItems(filterList, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        imageEdition.color(fullImage, filterList[which]);
                    }
                });

                accept.setVisibility(View.VISIBLE);

                alertDialogBuilder.show();

            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileOutputStream out = null;
                SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
                String d = dFormat.format(new Date());
                Bitmap bmp = ((BitmapDrawable) fullImage.getDrawable()).getBitmap();

                try {
                    out = new FileOutputStream(dirPath + "/"+ d + ".jpg");
                    bmp.compress(Bitmap.CompressFormat.JPEG, 70, out);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private String getRealPathFromImageURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

}
