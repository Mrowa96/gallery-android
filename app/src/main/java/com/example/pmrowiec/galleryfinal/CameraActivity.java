package com.example.pmrowiec.galleryfinal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.ViewManager;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CameraActivity extends AppCompatActivity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreviewClass cameraPreview;
    private FrameLayout frameLayout;
    private String path;
    private Button balanceBtn;
    private Button exposeBtn;
    private Button resolutionBtn;
    private ImageView selectImage = null;
    private ArrayList<ImageView> imageViews = new ArrayList<>();
    private int circleX;
    private int circleY;
    private float circleR;
    private Camera.Parameters camParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        initCamera();
        initPreview();
        CameraCircleView circle = new CameraCircleView(CameraActivity.this);
        frameLayout.addView(circle);

        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        circleX = width/2;
        circleY = height/2 - 100;
        circleR = (float) (width/3);
    }

    @Override
    public void onBackPressed() {
        camera.release();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, final Camera camera) {
            final byte[] fdata = data;
            // saveImage(fdata);
            BitmapFactory.Options options = new BitmapFactory.Options();    //opcje przekształcania bitmapy
            //options.inPurgeable = true;
            //options.outHeight = 100;
            // options.outWidth = 100;
            options.inSampleSize = 4;

            Bitmap bb = BitmapFactory.decodeByteArray(data, 0, data.length, options);
            bb = Bitmap.createScaledBitmap(bb, 100, 100, false);

            ImageView imageView = new ImageView(getApplicationContext());
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bb,bb.getWidth(),bb.getHeight(),true);
            Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);

            imageView.setImageBitmap(rotatedBitmap);
            final int sdk = android.os.Build.VERSION.SDK_INT;
           /* if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                imageView.setBackgroundDrawable( getResources().getDrawable(R.drawable.image_border) );
            } else {
                imageView.setBackground( getResources().getDrawable(R.drawable.image_border));
            }*/
            for(int i = 0; i < imageViews.size(); i++){
                ((ViewManager) imageViews.get(i).getParent()).removeView(imageViews.get(i));
            }
            imageViews.add(imageView);

            for(int i = 0; i < imageViews.size(); i++) {
                int x = (int) (circleX + (circleR * Math.cos(2 * Math.PI * i / imageViews.size())));
                int y = (int) (circleY + (circleR * Math.sin(2 * Math.PI * i / imageViews.size())));
                x -= 100;
                y -= 100;

                imageViews.get(i).setTag(i);
                imageViews.get(i).setLayoutParams(new FrameLayout.LayoutParams(200, 200));
                ((FrameLayout.LayoutParams) (imageViews.get(i).getLayoutParams())).setMargins(x, y, 0, 0);
                imageViews.get(i).setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        selectImage = (ImageView) v;

                        AlertDialog.Builder builderSingle = new AlertDialog.Builder(CameraActivity.this);
                        builderSingle.setTitle("Wybierz co chcesz zrobić");

                        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                                CameraActivity.this,
                                android.R.layout.simple_dropdown_item_1line);

                        arrayAdapter.add("Podglad");
                        arrayAdapter.add("Usun");
                        arrayAdapter.add("Zapisz");

                        builderSingle.setNegativeButton(
                                "Anuluj",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        builderSingle.setAdapter(
                                arrayAdapter,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case 0:
                                                Bitmap bmp = ((BitmapDrawable) selectImage.getDrawable()).getBitmap();
                                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                                byte[] byteArray = stream.toByteArray();

                                                camera.release();
                                                frameLayout = null;
                                                Intent intent = new Intent(CameraActivity.this, ShowPhotoActivity.class);
                                                intent.putExtra("picture", byteArray);
                                                startActivity(intent);

                                                break;
                                            case 1:
                                                int tag = (int) selectImage.getTag();
                                                imageViews.remove(tag);
                                                ((ViewManager) selectImage.getParent()).removeView(selectImage);
                                                break;

                                            case 2:
                                                AlertDialog.Builder placesBuilder = new AlertDialog.Builder(CameraActivity.this);
                                                placesBuilder.setTitle("Wybierz miejsce zapisu");

                                                final ArrayAdapter<String> placesAdapter = new ArrayAdapter<String>(
                                                        CameraActivity.this,
                                                        android.R.layout.simple_dropdown_item_1line);

                                                DirectoryClass dir = new DirectoryClass();
                                                File folder = dir.getMainDir();
                                                final File[] folders = folder.listFiles();

                                                for (final File fileEntry : folders) {
                                                    if (fileEntry.isDirectory()) {
                                                        placesAdapter.add(fileEntry.getName());
                                                    }
                                                }

                                                placesBuilder.setNegativeButton(
                                                        "Anuluj",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                dialog.dismiss();
                                                            }
                                                        });

                                                placesBuilder.setAdapter(
                                                        placesAdapter,
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                Bitmap bb = BitmapFactory.decodeByteArray(fdata, 0, fdata.length);
                                                                ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                                                                bb = Bitmap.createScaledBitmap(bb, 480, 800, false);

                                                                Matrix matrix = new Matrix();
                                                                matrix.postRotate(90);
                                                                Bitmap bmp23 = Bitmap.createBitmap(bb, 0, 0, bb.getWidth(), bb.getHeight(), matrix, true);

                                                                bmp23.compress(Bitmap.CompressFormat.PNG, 100, stream1);
                                                                byte[] byteArray23 = stream1.toByteArray();

                                                                String path = folders[which].getAbsolutePath();

                                                                saveImage(byteArray23, path);
                                                            }
                                                        });

                                                placesBuilder.show();
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                });

                        builderSingle.show();
                        return false;
                    }
                });

                frameLayout.addView(imageViews.get(i));
            }

            camera.startPreview();
        }
    };

    private void showBalance(){
        camParams = camera.getParameters();
        final ArrayList<String> balanceList = (ArrayList<String>) camParams.getSupportedWhiteBalance();
        final String[] balanceArr = new String[balanceList.size()];
        balanceList.toArray(balanceArr);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.balance);
        alertDialogBuilder.setItems(balanceArr, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                camParams.setWhiteBalance(balanceArr[which]);
                camera.setParameters(camParams);
            }
        });

        alertDialogBuilder.show();
    }

    private void showExpose(){
        camParams = camera.getParameters();
        int minExposure = camParams.getMinExposureCompensation();
        int maxExposure = camParams.getMaxExposureCompensation();
        final ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_selectable_list_item);

        for (int i = minExposure; i < maxExposure; i++) {
            adapter.add(i);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.expose);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                camParams.setExposureCompensation(adapter.getItem(item));
                camera.setParameters(camParams);
            }
        });

        builder.show();
    }

    private void showResolution(){
        camParams = camera.getParameters();
        final List <Camera.Size> resolutions = camParams.getSupportedPictureSizes();

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_selectable_list_item);

        for (Camera.Size res : resolutions) {
            adapter.add(res.width + "x" + res.height);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.expose);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                camParams.setPictureSize(resolutions.get(item).width, resolutions.get(item).height);
            }
        });

        builder.show();
    }

    private void initPreview(){
        cameraPreview = new CameraPreviewClass(CameraActivity.this, camera);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayoutCamera);
        frameLayout.addView(cameraPreview);

        ImageView takePictureView = (ImageView)findViewById(R.id.photoCamera);
        takePictureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            camera.takePicture(null, null, camPictureCallback);
            }
        });

        ImageView backImageView = (ImageView)findViewById(R.id.cancelCamera);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button balanceBtn = (Button)findViewById(R.id.balanceBtn);
        balanceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBalance();
            }
        });

        Button exposeBtn = (Button)findViewById(R.id.exposeBtn);
        exposeBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showExpose();
            }
        });

        Button resolutionBtn = (Button)findViewById(R.id.resolutionBtn);
        resolutionBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showResolution();
            }
        });
    }

    private void initCamera(){
        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if (!cam) {
            return;
        }
        else{
            camera = null;
            cameraId = getCameraId();

            if (cameraId < 0) {

            }
            else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
                camera.setDisplayOrientation(90);
            }
            else {
                camera = Camera.open();
                camera.setDisplayOrientation(90);
            }
        }

    }

    private int getCameraId(){
        int camerasCount = Camera.getNumberOfCameras();
        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == 0) {
                return i;
            }
            else if (cameraInfo.facing == 1) {
                return i;
            }
        }

        return 100;
    }

    private void saveImage(byte[] data, final String path){

        final byte[] dData= data;
        new Thread(new Runnable(){
            SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String d = dFormat.format(new Date());

            File myFoto = new File(path + "/"+ d + ".jpg");
            FileOutputStream fs = null;

            @Override
            public void run() {
                try {
                    fs = new FileOutputStream(myFoto);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(dData, 0, dData.length);

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, fs);
                    fs.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }).start();
    }

}
