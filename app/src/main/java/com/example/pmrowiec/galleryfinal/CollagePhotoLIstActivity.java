package com.example.pmrowiec.galleryfinal;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class CollagePhotoLIstActivity extends AppCompatActivity {

    private int inRow = 2;
    private int rowHeight = 200;
    private int dWidth;
    private int cellWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);

        TableLayout photoList = (TableLayout) findViewById(R.id.photoList);

        final Intent intent = getIntent();
        String dirpath = intent.getStringExtra("dirPath");

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        dWidth = size.x;

        DirectoryClass dir = new DirectoryClass(dirpath, true);

        final List<File> files = dir.listFiles();
        int rowsLength = (int) Math.ceil(files.size() / inRow) +1;
        int lastFile = 1;

        for(int r = 0; r < rowsLength; r++){
            TableRow row = new TableRow(this);
            TableRow.LayoutParams tableLayout = new TableRow.LayoutParams(
                    TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT
            );

            row.setLayoutParams(tableLayout);

            for(int i = lastFile; i <= files.size(); i++){
                if(i % 2 == 0){
                    cellWidth = (dWidth/3)*2;
                }
                else{
                    cellWidth = dWidth/3;
                }

                LinearLayout wrap = new LinearLayout(CollagePhotoLIstActivity.this);
                ImageView image = new ImageView(CollagePhotoLIstActivity.this);

                TableRow.LayoutParams wrapLayout = new TableRow.LayoutParams(
                        cellWidth,
                        rowHeight
                );

                TableRow.LayoutParams imageLayout = new TableRow.LayoutParams(
                        TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT
                );
                wrapLayout.setMargins(10, 10, 10, 10);

                wrap.setLayoutParams(wrapLayout);
                wrap.setGravity(Gravity.CENTER);
                image.setLayoutParams(imageLayout);

                Bitmap bitmap = BitmapFactory.decodeFile(files.get(i-1).getAbsolutePath());
                final String filePath = files.get(i-1).getAbsolutePath();
                image.setImageBitmap(bitmap);
                image.setScaleType(ImageView.ScaleType.FIT_XY);
                image.setTag(i);

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.putExtra("image", filePath);
                        setResult(777, intent);

                        finish();
                    }
                });

                wrap.addView(image);
                row.addView(wrap);

                if(i % inRow == 0){
                    lastFile = i+1;
                    break;
                }
            }

            photoList.addView(row);
        }
    }

}
