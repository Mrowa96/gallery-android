package com.example.pmrowiec.galleryfinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.HashMap;

public class CollageActivity extends AppCompatActivity {

    private ArrayList<HashMap<String, Integer>> list = new ArrayList<HashMap<String,Integer>>();
    private int dHeight;
    private int dWidth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        this.dHeight = displaymetrics.heightPixels;
        this.dWidth = displaymetrics.widthPixels;

        setButons();
    }

    private void setButons() {
        ImageView kolazImageView1 = (ImageView) findViewById(R.id.kolazImageView1);
        ImageView kolazImageView2 = (ImageView) findViewById(R.id.kolazImageView2);
        ImageView kolazImageView3 = (ImageView) findViewById(R.id.kolazImageView3);
        ImageView kolazImageView4 = (ImageView) findViewById(R.id.kolazImageView4);

        kolazImageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollageActivity.this, CollageTemplateActivity.class);
                addToList(1);
                intent.putExtra("lista", list);
                startActivity(intent);
            }
        });

        kolazImageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollageActivity.this, CollageTemplateActivity.class);
                addToList(2);
                intent.putExtra("lista", list);
                startActivity(intent);
            }
        });

        kolazImageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollageActivity.this, CollageTemplateActivity.class);
                addToList(3);
                intent.putExtra("lista", list);
                startActivity(intent);
            }
        });

        kolazImageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CollageActivity.this, CollageTemplateActivity.class);
                addToList(4);
                intent.putExtra("lista", list);
                startActivity(intent);
            }
        });
    }

    private void addToList(int listNum){
        list.clear();

        switch(listNum){
            case 1:
                HashMap<String, Integer> mapa01a = new HashMap<String, Integer>();

                mapa01a.put("x", 0);
                mapa01a.put("y", 0);
                mapa01a.put("width", this.dWidth/2);
                mapa01a.put("height", this.dHeight);

                HashMap<String, Integer> mapa01b = new HashMap<String, Integer>();

                mapa01b.put("x", this.dWidth/2);
                mapa01b.put("y", 0);
                mapa01b.put("width", this.dWidth/2);
                mapa01b.put("height", this.dHeight);

                list.add(mapa01a);
                list.add(mapa01b);

                break;
            case 2:
                HashMap<String, Integer> mapa02a = new HashMap<String, Integer>();

                mapa02a.put("x", 0);
                mapa02a.put("y", 0);
                mapa02a.put("width", this.dWidth);
                mapa02a.put("height", this.dHeight/2);

                HashMap<String, Integer> mapa02b = new HashMap<String, Integer>();

                mapa02b.put("x", 0);
                mapa02b.put("y", this.dHeight/2);
                mapa02b.put("width", this.dWidth);
                mapa02b.put("height", this.dHeight/2);

                list.add(mapa02a);
                list.add(mapa02b);

                break;

            case 3:
                HashMap<String, Integer> mapa03a = new HashMap<String, Integer>();

                mapa03a.put("x", 0);
                mapa03a.put("y", 0);
                mapa03a.put("width", this.dWidth);
                mapa03a.put("height", this.dHeight/3);

                HashMap<String, Integer> mapa03b = new HashMap<String, Integer>();

                mapa03b.put("x", 0);
                mapa03b.put("y", this.dHeight/3);
                mapa03b.put("width", this.dWidth);
                mapa03b.put("height", this.dHeight/3);

                HashMap<String, Integer> mapa03c = new HashMap<String, Integer>();

                mapa03c.put("x", 0);
                mapa03c.put("y", (this.dHeight/3) * 2);
                mapa03c.put("width", this.dWidth);
                mapa03c.put("height", this.dHeight/3);

                list.add(mapa03a);
                list.add(mapa03b);
                list.add(mapa03c);

                break;

            case 4:
                HashMap<String, Integer> mapa04a = new HashMap<String, Integer>();

                mapa04a.put("x", 0);
                mapa04a.put("y", 0);
                mapa04a.put("width", this.dWidth/2);
                mapa04a.put("height", this.dHeight/2);

                HashMap<String, Integer> mapa04b = new HashMap<String, Integer>();

                mapa04b.put("x", this.dWidth/2);
                mapa04b.put("y", 0);
                mapa04b.put("width", this.dWidth/2);
                mapa04b.put("height", this.dHeight/2);

                HashMap<String, Integer> mapa04c = new HashMap<String, Integer>();

                mapa04c.put("x", 0);
                mapa04c.put("y", this.dHeight/2);
                mapa04c.put("width", this.dWidth/2);
                mapa04c.put("height", this.dHeight/2);

                HashMap<String, Integer> mapa04d = new HashMap<String, Integer>();

                mapa04d.put("x", this.dWidth/2);
                mapa04d.put("y", this.dHeight/2);
                mapa04d.put("width", this.dWidth/2);
                mapa04d.put("height", this.dHeight/2);

                list.add(mapa04a);
                list.add(mapa04b);
                list.add(mapa04c);
                list.add(mapa04d);

                break;
        }
    }
}
