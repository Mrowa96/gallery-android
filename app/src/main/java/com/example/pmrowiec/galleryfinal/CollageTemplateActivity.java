package com.example.pmrowiec.galleryfinal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CollageTemplateActivity extends AppCompatActivity {

    private ImageView current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_template);

        ArrayList<HashMap<String, Integer>> lista = (ArrayList<HashMap<String, Integer>>) getIntent().getSerializableExtra("lista");

        for (int i = 0; i < lista.size(); i++) {
            final ImageView imageView = new ImageView(this);
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.collageTemplate);

            imageView.setX(lista.get(i).get("x"));
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.photo));
            imageView.setY(lista.get(i).get("y"));
            imageView.setLayoutParams(new FrameLayout.LayoutParams(lista.get(i).get("width"), lista.get(i).get("height")));
            imageView.setTag(i);
            imageView.setScaleType(ImageView.ScaleType.CENTER);

            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    current = (ImageView) v;

                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(CollageTemplateActivity.this);
                    builderSingle.setTitle("Wybierz");

                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                            CollageTemplateActivity.this,
                            android.R.layout.simple_dropdown_item_1line);

                    arrayAdapter.add("Aparat");
                    arrayAdapter.add("Galeria");

                    builderSingle.setNegativeButton(
                            "Anuluj",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    builderSingle.setAdapter(
                            arrayAdapter,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case 0:
                                            Intent photoIntent = new Intent(CollageTemplateActivity.this, CollageCameraActivity.class);
                                            startActivityForResult(photoIntent, 1);
                                            break;
                                        case 1:
                                            Intent dirIntent = new Intent(CollageTemplateActivity.this, CollageDirectoryListActivity.class);
                                            startActivityForResult(dirIntent, 0);
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            });

                    builderSingle.show();
                }
            });

            frameLayout.addView(imageView);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data.hasExtra("xdata")){
            Bundle extras = data.getExtras();
            byte[] xdata = (byte[]) extras.get("xdata");

            Bitmap bmp = BitmapFactory.decodeByteArray(xdata, 0, xdata.length);
            current.setImageBitmap(bmp);
        }
        else if(data.hasExtra("image")){
            Bundle extras = data.getExtras();
            String path = (String) extras.get("image");

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bmp = BitmapFactory.decodeFile(path, bmOptions);

            current.setImageBitmap(bmp);
        }
    }

}