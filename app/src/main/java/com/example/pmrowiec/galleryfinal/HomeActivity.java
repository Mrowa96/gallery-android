package com.example.pmrowiec.galleryfinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        createDefaultFolders();
        addListeners();
    }

    private void addListeners() {
        Typeface fontType = Typeface.createFromAsset(getAssets(),"fonts/fontawesome-webfont.ttf");

        RelativeLayout directoryBtn = (RelativeLayout) findViewById(R.id.directoryBtn);
        TextView textView = (TextView) findViewById(R.id.textView);
        RelativeLayout cameraBtn = (RelativeLayout) findViewById(R.id.cameraBtn);
        TextView textView2 = (TextView) findViewById(R.id.textView2);
        RelativeLayout collageBtn = (RelativeLayout) findViewById(R.id.collageBtn);
        TextView textView3 = (TextView) findViewById(R.id.textView3);

        textView.setTypeface(fontType);
        textView2.setTypeface(fontType);
        textView3.setTypeface(fontType);

        directoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent directoryIntent = new Intent(HomeActivity.this, DirectoryListActivity.class);
                startActivity(directoryIntent);
            }
        });

        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent directoryIntent = new Intent(HomeActivity.this, CameraActivity.class);
                startActivity(directoryIntent);
            }
        });

        collageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent directoryIntent = new Intent(HomeActivity.this, CollageActivity.class);
                startActivity(directoryIntent);
            }
        });
    }

    private void createDefaultFolders(){
        DirectoryClass dirs = new DirectoryClass();

        dirs.createDefault();
    }
}
