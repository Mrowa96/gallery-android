package com.example.pmrowiec.galleryfinal;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ShowPhotoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_photo);

        ImageView photoPreview = (ImageView) findViewById(R.id.photoPreview);
        Intent intent = getIntent();
        byte[] photo = intent.getByteArrayExtra("picture");

        Bitmap bitmap = BitmapFactory.decodeByteArray(photo, 0, photo.length);
        photoPreview.setImageBitmap(bitmap);
    }
}
