package com.example.pmrowiec.galleryfinal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CollageCameraActivity extends Activity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreviewClass cameraPreview;
    private FrameLayout frameLayout;
    private String path;
    private Button balanceBtn;
    private Button exposeBtn;
    private Button resolutionBtn;
    private Camera.Parameters camParams;

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            Intent intent = new Intent();
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
            Bitmap bitmapFinal = Bitmap.createScaledBitmap(bmp, 640, 480, false);
            bitmapFinal.compress(Bitmap.CompressFormat.JPEG, 40, out);

            try {
                out.close();
                intent.putExtra("xdata", out.toByteArray());
                setResult(777, intent);

            } catch (IOException e) {
                e.printStackTrace();
            }

            camera.release();
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_camera);

        initCamera();
        initPreview();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void showBalance(){
        camParams = camera.getParameters();
        final ArrayList<String> balanceList = (ArrayList<String>) camParams.getSupportedWhiteBalance();
        final String [] balanceArr = new String[balanceList.size()];
        balanceList.toArray( balanceArr );

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.balance);
        alertDialogBuilder.setItems(balanceArr, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                camParams.setWhiteBalance(balanceArr[which]);
                camera.setParameters(camParams);
            }
        });

        alertDialogBuilder.show();
    }

    private void showExpose(){
        camParams = camera.getParameters();
        int minExposure = camParams.getMinExposureCompensation(); // minimalna kompensacja naswietlenia
        int maxExposure = camParams.getMaxExposureCompensation(); // maksymalna kompensacja naswietlenia
        final ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_selectable_list_item);

        for(int i = minExposure; i< maxExposure; i++){
            adapter.add(i);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.expose);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                camParams.setExposureCompensation(adapter.getItem(item));
                camera.setParameters(camParams);
            }
        });

        builder.show();
    }

    private void showResolution(){
        camParams = camera.getParameters();
        final List <Camera.Size> resolutions = camParams.getSupportedPictureSizes();

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_selectable_list_item);

        for(Camera.Size res : resolutions){
            adapter.add(res.width + "x" + res.height);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.expose);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                camParams.setPictureSize(resolutions.get(item).width, resolutions.get(item).height);
            }
        });

        builder.show();
    }

    private void initPreview(){
        cameraPreview = new CameraPreviewClass(CollageCameraActivity.this, camera);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayoutCamera);
        frameLayout.addView(cameraPreview);

        ImageView takePictureView = (ImageView)findViewById(R.id.photoCamera);
        takePictureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.takePicture(null, null, camPictureCallback);
            }
        });

        Button balanceBtn = (Button)findViewById(R.id.balanceBtn);
        balanceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBalance();
            }
        });

        Button exposeBtn = (Button)findViewById(R.id.exposeBtn);
        exposeBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showExpose();
            }
        });

        Button resolutionBtn = (Button)findViewById(R.id.resolutionBtn);
        resolutionBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                showResolution();
            }
        });
    }

    private void initCamera(){
        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if (!cam) {
            return;
        }
        else{
            camera = null;
            cameraId = getCameraId();
            // jest jakaś kamera!
            if (cameraId < 0) {
                // brak kamery z przodu!
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
                camera.setDisplayOrientation(90);
            } else {
                camera = Camera.open();
                camera.setDisplayOrientation(90);
            }
        }

    }

    private int getCameraId(){
        int camerasCount = Camera.getNumberOfCameras();
        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == 0) {
                return i;
            }
            else if (cameraInfo.facing == 1) {
                return i;
            }
        }
        return 100;
    }
}
