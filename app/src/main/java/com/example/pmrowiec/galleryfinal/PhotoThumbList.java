package com.example.pmrowiec.galleryfinal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

public class PhotoThumbList extends View {

    private final ImageView thumb;

    public PhotoThumbList(final Context context, String path, int type){
        super(context);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        ImageView image = new ImageView(context);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(path,bmOptions);

        switch(type){
            case 0:
                bitmap = Bitmap.createScaledBitmap(bitmap, display.getWidth() /3,100,true);
                break;
            case 1:
                bitmap = Bitmap.createScaledBitmap(bitmap,display.getWidth() * 2/3, 100,true);
                break;

            default:
                bitmap = Bitmap.createScaledBitmap(bitmap,display.getWidth() * 2/3, 100,true);
                break;
        }

        image.setImageBitmap(bitmap);

        this.thumb = image;
    }

    public ImageView getThumb(){
        return thumb;
    }
}
