package com.example.pmrowiec.galleryfinal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;

public class PreviewTextClass extends View {

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    int color1;
    int color2;
    String text;

    public PreviewTextClass(Context context, int fontSize, Typeface font, String text, int color1, int color2){
        super(context);

        paint.reset();
        paint.setAntiAlias(true);
        paint.setTextSize(60);
        paint.setTypeface(font);

        this.color1 = color1;
        this.color2 = color2;
        this.text = text;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color1);
        canvas.drawText(this.text, 10, 100, paint);
    Log.d("kolor1", String.valueOf(color2));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        paint.setColor(color2);
        canvas.drawText(this.text, 10, 100, paint);
    }
}
